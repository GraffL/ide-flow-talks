# Navigation #

* Vor/Zurück mit Pfeiltasten <- / -> (oder mit Presenter)
* Bei Maushover erscheint rechts unten eine Navigation, die direktes Springen zu einzelnen Folien erlaubt (Auswahl aus Dropdown-Menü)
* "T" springt zu Titel-Folie, "O" zu Schlussfolie
* In Schlussfolie springt Klick auf einzelne Folie zu dieser


# Talk Overview #

(bullet points correspond to individual steps)

## Title-Slide ##

## Slide 1 (Problem Statement) ##

First: Static flows:
* example of path-based static flow
* example of edge-based static flow
They are connected. 
* From path-based to edge-based: Adding up (easy)
* From edge-based to path-based: flow decomposition (theorem). Note: must allow cylces in decompostion.
Proof by providing decomposition algorithm:
* Input: edge based s,d-flow
* Fix some order of all s,d-paths
* From each path: Remove as much flow as possible
In example:
* On p1: can remove 2
* remove 
* On p2
* remove 5
* On p3
* remove 2
* On p4: Noting left
It remains flow on a cycle (i.e. a circulation)

Now: Switch to dynamic flows:
* Can again be described path-based (with inflow rate function for each path)
* example animation
* Or as edge-based flow (with inflow rate into each edge)
* If travel times on edges are fixed and constant, then connection between both types is again adding up with time shift
* However, travel times are typically time and flow dependent (e.g. change with congestion caused by flow -> animation for queues)
* Then edge-flow dependent travel times D(g,.)
* induce arrival times 
* More complex connection between path- and edge-based flows
* In particular: Edge-based flow appears on both sides of the equation
Hence, even the direction: Path-based -> edge-based is now non-trivial, and gets its own name:
* network-loading
* well studied for various specific flow models (and even some general setting: Koch, XWFMZ?)
We are interested in the reverse direction, i.e. flow decomposition of dynamic flows (much less studied, only very briefly in Koch thesis). 
First observation:
* Need now allow walk containing cycles (not just simple paths as in static case)
* Animation of dynamic flow that has not decomposition in just simple paths and cycles
Idea for proofing existence of dynamic decomposition: Use natural decomposition algorithm
* Adjust static version
Three difficulties:
* What do we mean by: The remaining flow at the end (i.e. difference between walk-based and edge-based flow!) is (dynamic) circulation? -> Need to define dynamic circulation and (more important) subtracting walk-based flow from edge-based flow (for dynamic setting)
* Maximal amount of flow? What exactly does this mean? Does this maximum exist? -> find and analyze suitable optimization problem
* How do we remove this flow (similar to difference problem)

Central idea (in particular for comparing edge and walk-based flows, etc): 
* Fix the travel times induced by the given edge-based flow g and keep them throughout the algorithm (even as g changes!) -> autonomous network loading (i.e. network loading/flow decomposition/flows with respect to travel times induced by another flow (or just given from somewhere) - only require absolute continuity of D and non-decreasing of T (=arrival time at end of edges))
* So, consider autonomous edge-flows/network loading/flow decomposition
* This is all necessary setup.

## Slide 2 (Challenges/Main Theorem) ##

* So, the two main challenges to showing dynamic flow decomposition are: definition and existence of max. removable flow (-> opt. problem) / correctness of algorithm (-> via invariants)
* Our main theorem is ...

## Slide 3 (Well-Definedness) ##

We start with the opt problem which is of this form
* More specifically, we need this opt problem: Max amount of flow sent along a given walk w, such that we don't use any edge on the walk more than the currently remaining edge-flow allows for. Here, D_w is the set of all walk flows which lead to a feasible edge flow (under the fixed autonomous network loading). These walk-inflows can be characterized as those flows that never send flow during times where any of the edge arrival times on this walk are non-decreasing
* So, if this is the arrival time function for some edge
* No flow may use the walk such that it would arrive at this edge during the red intervals
Now, we have to show that this opt. problem has an optimal solution. 
* In fact we do this for the slightly more general, abstract opt. problem (P)
* To show existence of an optimal solution, we mainly have to show that the feasibility space is weakly closed (Lemma)
The proof of this lemma rests on two main aspects:
* The characterization of walk-inflows leading to feasible edge-flows
* The fact that the (autonomous) network-loading is weakly upper semicontinous
With this we get the following theorem
* Existence of opt solutions for P (and hence during the flow decomp alg)

## Slide 4 (Correctness) ##

* Need the lemma: Autonomous edge-flow - autonomous network loading of walk inflow is again autonomous edge-flow 
* This is the invariant during our decomp algorithm.
To see that the end result is correct, we need the following theorem:
* Any autonomous edge-flow 
* either has at least one flow carrying s,d-walk (from which we, therefore, could still remove flow)
* or is a dynamic circulation (hence we are done)
And
* the former is the case iff there is no outflow from the source
* and the latter otherwise
Together, this now shows that the decomposition algorithm is correct

## Slide 5 (Main Thm/Formal Algorithm) ##

Here now the formal decomposition algorithm
* At the start we fix the travel times induced by the given $g$ and consider all flows from now on as autonomous flow wrt. these (fixed) travel times
* In each iteration we determine the maximal amount of flow left on one of the walks (by solving the opt. problem)
* and then remove it from the edge based flow g
The invariant now ensures that g always stays an autonomous flow. At the "end" (after potentially infinitely many steps) there can be no walk with flow left (as we have remove the maximal amount from each) and hence the correctness thm ensures that the remaining flow is a dynamic circulation.
* This gives us our main theorem (correctness of decomp alg / existence of flow decomp)

## Slide 6 (Further results/Open questions) ##

We also provide a characterization of edge flows that allow a decomposition in only walk (but no cycles)
* and observe that the algorithm is "finite" (finitely many iterations) if there is some lower bound on edge traversal times and the flow has finite support

Some open questions:
* computational complexity of determining flow decomp (for specific flow models (e.g. Vickrey) and certain "simple" flows (e.g. piecewise constant))
* Characterization of of existence of path-decompositions (i.e. without using walks)

## Slide 7 (Overview) ##