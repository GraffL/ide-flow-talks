/* Goto TitlePage und Goto Overview */

(function ( document, window ) {
    'use strict';
    // wait for impress.js to be initialized
    document.addEventListener("impress:init", function (event) {
        var api = event.detail.api;
        var gc = api.lib.gc;
        
        gc.addEventListener(document, "keyup", function ( event ) {
			// 'o' springt direkt zur �berblick-Folie
			if ( event.keyCode === 79) {
				api.goto("Overview")
			}
			// 't' springt direkt zum Anfang
			if ( event.keyCode === 84) {
				api.goto("Title")
			}
			// 'c' oeffnet und schliesst Canvas
			if ( event.keyCode === 67) {
				if(document.getElementById("DrawingCanvas").style.display === "none") {
					document.getElementById("DrawingCanvas").style.display = "block";
				} else {
					document.getElementById("DrawingCanvas").style.display = "none";
				}
			}
			if ( event.keyCode === 81) {
				if(document.getElementById("impress-toolbar").style.display === "none") {
					document.getElementById("impress-toolbar").style.display = "block"
				} else {
					document.getElementById("impress-toolbar").style.display = "none"
				}
			}
			if ( event.keyCode === 49) {
				api.goto("Overview1")
			}
			if ( event.keyCode === 50) {
				api.goto("Overview2")
			}
			if ( event.keyCode === 51) {
				api.goto("Overview3")
			}
			if ( event.keyCode === 52) {
				api.goto("Overview")
			}
			/*if( event.keyCode == 65) {
				document.getElementById("impress-autoplay-playpause").click();
			}
			if ( event.keyCode === 69) {
				api.goto("Existence")
			}
			if ( event.keyCode === 71) {
				api.goto("Generalization")
			}
			if ( event.keyCode === 83) {
				api.goto("Structure")
			}
			if ( event.keyCode === 49) {
				api.goto("Algorithm")
			}
			if ( event.keyCode === 50) {
				api.goto("AlgorithmAcyclic")
			}
			if ( event.keyCode === 51) {
				api.goto("AlgorithmGeneral")
			}
			if ( event.keyCode === 52) {
				api.goto("OutputComplexity")
			}
			if ( event.keyCode === 53) {
				api.goto("NP-Hardness")
			}
			if ( event.keyCode === 49) {
				api.goto("Model")
			}
			if ( event.keyCode === 50) {
				api.goto("SS-Ex")
			}
			if ( event.keyCode === 51) {
				api.goto("SS-Term")
			}
			if ( event.keyCode === 52) {
				api.goto("SS-PoA")
			}
			if ( event.keyCode === 53) {
				api.goto("OutputComplexity")
			}
			if ( event.keyCode === 54) {
				api.goto("AlgorithmAcyclic")
			}
			if ( event.keyCode === 55) {
				api.goto("NP-Hardness")
			}
			if ( event.keyCode === 57) {
				api.goto("MS-Ex")
			}
			if ( event.keyCode === 48) {
				api.goto("MS-Term")
			}
			if ( event.keyCode === 48) {
				api.goto("Chapter-Conclusion")
			}*/
			
        }, false);

    }, false);
})(document, window);

(function (document, window) {
	'use strict'
	
	var afterOverview = function( event ) {
		
		if ( ( !event ) || ( !event.target ) ) {
      return;
		}
		
		if (event.detail.next.id == "Overview") {
			document.body.classList.add("finished");
		} else if (event.detail.next.id == "Title") {
			document.body.classList.remove("finished");
		} 
		
	}
	
	window.impress.addPreStepLeavePlugin( afterOverview );
	
})(document, window);