## Arbeitsseminar SoSe2020 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_AS.html) | [index_AS.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_AS.html): Vortrag im von Bene organisierten Arbeitsseminar in Augsburg.

Dauer: ca 1 1/2h (mit vielen Zwischenfragen, aber auch Überspringen der Folien zu Single-Sink Termination und PoA)

## Dagstuhl 2020 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_Dagstuhl.html) | [index_Dagstuhl.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_Dagstuhl.html): Vortrag im Seminar "[Mathematical Foundations of Dynamic Nash Flows](https://www.dagstuhl.de/de/programm/kalender/evhp/?semnr=20394)" 

Dauer: ca 50min (kurze Zwischenfragen, physikalisches Modell bereits bekannt)

## OMS 2020 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_OMS.html) | [index_OMS.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_OMS.html): Vortrag im OMS-Oberseminar der RWTH Aachen.

Dauer: ca 55min (NP-Hardness weggelassen, Mult-Sink-Folie auch)

## WINE 2020 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_WINE.html) | [index_WINE.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_WINE.html): Vortrag für die WINE 2020 in Peking

Dauer: 25min (ohne Unterbrechung, zügig) 
Schriftgröße ist etwas klein.

## IPCO 2021 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_IPCO2021.html) | [index_IPCO2021.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_IPCO2021.html): Vortrag für die IPCO2021 in Atlanta

Dauer: 27min (ohne Unterbrechung, zügig) 

## Dagstuhl 2022 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_Dagstuhl22.html) | [index_Dagstuhl22.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_Dagstuhl22.html): Vortrag im Seminar "[Dynamic Traffic Models in Transportation Science](https://www.dagstuhl.de/en/program/calendar/semhp/?semnr=22192)"

Dauer: 25min (mit Zwischenfragen, ohne Erklärung des Algorithmus und ohne Zusatzslide) 

## Graduate Get Together (Augsburg) 2022 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_Graduate.html) | [index_Graduate.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_Graduate.html): Vortrag beim Graduate Get Together der MNTF am 26.6.22

Dauer: etwa 20min? (mit Zwischenfragen) 


## ATMOS 2022 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_ATMOS.html) | [index_ATMOS.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_ATMOS.html): Vortrag auf "ATMOS 2022"

Dauer: 18min (ohne Zusatzfolien, ohne Graphen zu verschiedenen Recharging-Kapazitäten) 


## DCGT 2023 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_DCGT23.html) | [index_OS23.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_DCGT23.html): Vortrag auf "13th Day of Computational Game Theory" (Amsterdam)

Dauer: 18min (nur Titel und Model, ohne BSDE/LPDE, Existenz und Conclusion)


## Group Workshop 2023 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_OS23.html) | [index_OS23.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_OS23.html): Vortrag im Group workshop in unserem Oberseminar

Dauer: 11min (Ohne Open Questions und Results sehr schnell, dafür zwei Zwischenfragen)


## EC 2023 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_EC23.html) ([PDF](https://graffl.gitlab.io/ide-flow-talks/index_EC23.pdf) | [index_EC23.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_EC23.html): Vortrag auf EC'23

Dauer: 16min (Ohne Beweis zu Existenz-Thm)


## Thesis Defense 2023 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_Defense.html) ([index_Defense.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_Defense.html): Vortrag bei Verteidigung

Dauer: ~35min (ohne Folie zur beschränkung der Anzahl an Erweiterungen)


## Dagstuhl Seminar 2024 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_Dagstuhl24.html) ([index_Dagstuhl24.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_Dagstuhl24.html): Vortrag im Seminar "[Dynamic Traffic Models in Transportation Science](https://www.dagstuhl.de/24281)"

Dauer: ~25-30min (mit einigen Zwischenfragen)


## OR 2024 ##

[Slides](https://graffl.gitlab.io/ide-flow-talks/index_OR24.html) ([index_OR24.html](https://gitlab.com/GraffL/ide-flow-talks/-/blob/master/public/index_OR24.html): Vortrag auf OR 2024

Dauer: ~20-25min (ohne Slides zu ManyPhases, Non-Termination)

Dauer: ~35min (ohne Folie zur beschränkung der Anzahl an Erweiterungen)